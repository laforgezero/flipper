# CrawlerAPI
É a API para iniciar a busca assincrona por legendas no site *legendas.tv*, e obter o resultado dessa busca.

## Executar o container RabbitMQ
**docker run -d -p 5672:5672 --hostname rabbit --name rabbit rabbitmq**

## Fazer o build do container
**docker builder build . -t crawler_api:latest**

## Executar o container
**docker run --rm --env AMQP_HOSTNAME=192.168.0.42 -p 3000:3000 -it  crawler_api:latest**

## Para iniciar uma consulta
1. Abrir o endereço **http://localhost:3000/search/The%20Simpsons**
2. Copiar o **Token** que retorna

## Para receber o resultado
1. Abrir o endereço, substituindo o Token, **http://localhost:3000/legends/{Token}**