const config = require('./config');
const utils = require('./utils');
const util = require('util');
const rabbitmq = require('./rabbitmq')
const express = config.express;
const app = express()
const moduleName = 'Legend API'

app.get('/search/:search', (req, res) => {
    utils.log(moduleName, `Pesquisando '${req.url}'`);
    const search = req.params.search;
    const key = utils.uuidv4();
    rabbitmq.sendToQueue(config, JSON.stringify({ search: search, key: key }))
    res.send(key);
})

app.get('/legends/:key', async (req, res) => {
    utils.log(moduleName, `Pesquisando '${req.url}'`);
    const key = req.params.key;
    const ret = await rabbitmq.getQueue(config, key);
    res.send(ret);
})

app.listen(config.api.port, () => {
    utils.log(moduleName, `Ouvindo o endereço 'http://localhost:${config.api.port}'`);
})