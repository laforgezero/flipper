const Log = require('./utils').log;
const moduleName = 'RabbitMQ';
var channel, queueName;
var messages = [];

function sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
} 

async function getQueue(config, queue) {
    messages = [];
    consumeQueue(config, queue);
    await sleep(config.api.waitTime);
    const msgs = messages.map(item => JSON.parse(item.content.toString()));
    const header = msgs.find(item => item.length);
    const itens = msgs.filter(item => !item.length);
    var ret = { };
    if (header && itens) {
        ret['mensagem'] = `Encontradas ${itens.length} de ${header.length} legendas.`;
        ret['quantidadeLegendas'] = itens.length;
        ret['totalLegendas'] = header.length;
        ret['legendas'] = itens;
        Log(moduleName, ret['mensagem']);
        if (header.length === itens.length)
            channel.ackAll();
        else
            channel.nackAll();
    } else {
        ret['mensagem'] = 'Não foram encontradas legendas.';
    }
    channel.close();
    return ret;
}

function connectRabbit(config, callback) {
    const amqp = config.amqp;
    const util = config.util;
    if (channel)
        callback(channel);
    else {
        var urlRabbit = util.format('amqp://%s:%s@%s:%s/%s', config.rabbitmq.user, config.rabbitmq.pass, config.rabbitmq.hostname, config.rabbitmq.port, config.rabbitmq.vhost);
        Log("RabbitMQ", util.format("Conectando ao servidor %s", urlRabbit));
        amqp.connect(urlRabbit, function(err, conn) {
            if (err) {
                Log(moduleName, util.format('Erro ao conectar no servidor: %s', err));
            } else {
                conn.createChannel(function(err, ch) {
                    ch.on('close', () => channel = null);
                    channel = ch;
                    if (err) {
                        Log(moduleName, util.format('Erro ao criar o canal %s', err));
                    } else {
                        callback(ch);
                    }
                });
            }
        });
    }
    return null;
}

async function consumeQueue(config, queue) {
    const util = config.util;
    queueName = queue || config.rabbitmq.queueName;
    await connectRabbit(config, async function(ch) {
        await ch.assertQueue(queueName, { exclusive: false }, async function(err, q) {
            if (err) {
                Log(moduleName, util.format('Erro ao consumir o canal: %s', err));
            } else {
                Log(moduleName, util.format("Aguardando mensagens em %s. Para sair, pressione CTRL+C", q.queue));
                ch.prefetch(1000);
                await ch.consume(q.queue, async function(msg) {
                    // Log(moduleName, util.format("Mensagem recebida: %s", msg.content.toString()));
                    try {
                        if (msg)
                            messages.push(msg);
                    } catch (err) {
                        Log(moduleName, util.format('Erro ao processar a mensagem: %s', err));
                        ch.nack(msg);
                    }
                }, {noAck: false});
            }
        });    
    });
}

function sendToQueue(config, message, queue) {
    const util = config.util;
    const msg = (typeof message === 'string') ? message : JSON.stringify(message);
    const properties = {
        contentType: "application/json",
        contentEncoding: "UTF-8",
        appId: "Google-Interface",
        persistent: true
    };
    connectRabbit(config, function(ch) {
        const queueName = queue || config.rabbitmq.queueName;
        ch.assertQueue(queueName, {exclusive: false});
        ch.publish('', queueName, Buffer.from(msg), properties);
    });
}

module.exports = {
    consumeQueue: consumeQueue,
    sendToQueue, sendToQueue,
    getQueue: getQueue
}