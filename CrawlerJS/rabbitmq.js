const Log = require('./utils').log;
const moduleName = 'RabbitMQ';
var channel;

function connectRabbit(config, callback) {
    const amqp = config.amqp;
    const util = config.util;
    if (channel)
        callback(channel);
    else {
        var urlRabbit = util.format('amqp://%s:%s@%s:%s/%s', config.rabbitmq.user, config.rabbitmq.pass, config.rabbitmq.hostname, config.rabbitmq.port, config.rabbitmq.vhost);
        Log("RabbitMQ", util.format("Conectando ao servidor %s", urlRabbit));
        amqp.connect(urlRabbit, function(err, conn) {
            if (err) {
                Log(moduleName, util.format('Erro ao conectar no servidor: %s', err));
            } else {
                Log(moduleName, 'Conectado ao servidor');
                conn.createChannel(function(err, ch) {
                    channel = ch;
                    if (err) {
                        Log(moduleName, util.format('Erro ao criar o canal %s', err));
                    } else {
                        callback(ch);
                    }
                });
            }
        });
    }
    return null;
}

async function consumeQueue(config, consumeFunction, queue) {
    const util = config.util;
    const queueName = queue || config.rabbitmq.queueName;
    var options = { exclusive: false };
    if (queue)
        options['expires'] = 300000;
    await connectRabbit(config, async function(ch) {
        await ch.assertQueue(queueName, options, async function(err, q) {
            if (err) {
                Log(moduleName, util.format('Erro ao consumir o canal: %s', err));
            } else {
                Log(moduleName, util.format("Aguardando mensagens em %s. Para sair, pressione CTRL+C", q.queue));
                ch.prefetch(1);
                await ch.consume(q.queue, async function(msg) {
                    Log(moduleName, util.format("Mensagem recebida: %s", msg.content.toString()));
                    try {
                        if (consumeFunction)
                            await consumeFunction(msg.content);
                        ch.ack(msg);
                    } catch (err) {
                        Log(moduleName, util.format('Erro ao processar a mensagem: %s', err));
                        ch.nack(msg);
                    }
                }, {noAck: false});
            }
        });    
    });
}

function sendToQueue(config, message, queue) {
    const util = config.util;
    const msg = (typeof message === 'string') ? message : JSON.stringify(message);
    const properties = {
        contentType: "application/json",
        contentEncoding: "UTF-8",
        appId: "Google-Interface",
        persistent: true
    };
    if (queue)
        properties['expires'] =  300000;
    connectRabbit(config, function(ch) {
        const queueName = queue || config.rabbitmq.queueName;
        ch.assertQueue(queueName, {exclusive: false});
        ch.publish('', queueName, Buffer.from(msg), properties);
    });
}

module.exports = {
    consumeQueue: consumeQueue,
    sendToQueue, sendToQueue
}