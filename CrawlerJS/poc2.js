const config = require("./config");
const rabbitmq = require('./rabbitmq');
const utils = require('./utils');

rabbitmq.sendToQueue(config, { length: 10 }, 'Teste');

setTimeout(function() {
    for (let i=0; i < 5; i++) {
        const msg = JSON.stringify({ search: 'The Simpsons', key: utils.uuidv4() })
        rabbitmq.sendToQueue(config, msg, 'Teste');
    }
}, 100);

setTimeout(function() {
    for (let i=0; i < 5; i++) {
        const msg = JSON.stringify({ search: 'The Simpsons', key: utils.uuidv4() })
        rabbitmq.sendToQueue(config, msg, 'Teste');
    }
}, 10000);
