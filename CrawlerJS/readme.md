# CrawlerJS
É o robo que realiza a pesquisa das legendas no site *legendas.tv* e em seguida obtem os detalhes de cada legenda.

## Executar o container RabbitMQ
**docker run -d -p 5672:5672 --hostname rabbit --name rabbit rabbitmq**
## Fazer o build do container:
**docker builder build . -t crawler_js:latest**

### Executar o container:
**docker run --rm --env LEGENDAS_USER=fliperapp --env LEGENDAS_PASSWORD=123456 --env AMQP_HOSTNAME=192.168.0.42 -it crawler_js:latest**