const config = require('./config');
const Log = require('./utils').log;
const moduleName = 'RabbitMQ';
var channel;
var messages = [];
var timer = null;

function checkQueue() {
    const msgs = messages.map(item => JSON.parse(item.content.toString()));
    const header = msgs.find(item => item.length);
    const itens = msgs.filter(item => !item.length);
    console.log('header', header);
    console.log('itens', itens.length);
    if (header && itens && header.length === itens.length) {
        clearTimeout(timer);
        messages.forEach(item => channel.ack(item));
        messages = [];
        channel.deleteQueue('Teste');
        channel.close();
    }
}

function connectRabbit(config, callback) {
    const amqp = config.amqp;
    const util = config.util;
    if (channel)
        callback(channel);
    else {
        var urlRabbit = util.format('amqp://%s:%s@%s:%s/%s', config.rabbitmq.user, config.rabbitmq.pass, config.rabbitmq.hostname, config.rabbitmq.port, config.rabbitmq.vhost);
        Log("RabbitMQ", util.format("Conectando ao servidor %s", urlRabbit));
        amqp.connect(urlRabbit, function(err, conn) {
            if (err) {
                Log(moduleName, util.format('Erro ao conectar no servidor: %s', err));
            } else {
                Log(moduleName, 'Conectado ao servidor');
                conn.createChannel(function(err, ch) {
                    ch.on('close', () => channel = null);
                    channel = ch;
                    if (err) {
                        Log(moduleName, util.format('Erro ao criar o canal %s', err));
                    } else {
                        callback(ch);
                    }
                });
            }
        });
    }
    return null;
}

async function consumeQueue(config, consumeFunction, queue) {
    const util = config.util;
    const queueName = queue || config.rabbitmq.queueName;
    await connectRabbit(config, async function(ch) {
        await ch.assertQueue(queueName, {exclusive: false}, async function(err, q) {
            if (err) {
                Log(moduleName, util.format('Erro ao consumir o canal: %s', err));
            } else {
                Log(moduleName, util.format("Aguardando mensagens em %s. Para sair, pressione CTRL+C", q.queue));
                ch.prefetch(1000);
                await ch.consume(q.queue, async function(msg) {
                    try {
                        if (msg) {
                            Log(moduleName, util.format("Mensagem recebida: %s", msg.content.toString()));
                            messages.push(msg);
                            if (!timer)
                                timer = setInterval(checkQueue, 1000);
                            ch.ack(msg);
                            }
                    } catch (err) {
                        Log(moduleName, util.format('Erro ao processar a mensagem: %s', err));
                        ch.nack(msg);
                    }
                }, {noAck: false});
            }
        });    
    });
}

// consumeQueue(config, null, 'd81b7d09-468c-4de3-8e36-a8ba0718088e');
consumeQueue(config, null);